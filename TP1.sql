create database db1;

use db1;

create table client(
nom varchar(30),
prenom varchar(30),
age int not null,
telephone varchar(10),
mail varchar(30),
adresse varchar(40),
code_postal varchar(6),
ville varchar(20)
);

create table employe(
nom varchar(30),
prenom varchar(30),
age int not null,
telephone varchar(10),
mail varchar(30),
adresse varchar(40),
code_postal varchar(6),
ville varchar(20)
);

create table manager(
nom varchar(30),
prenom varchar(30),
age int,
telephone varchar(10),
mail varchar(30),
adresse varchar(40),
code_postal varchar(6),
ville varchar(20)
);

insert into client()values('Marco', 'Paul', 45, '0234567865', 'laurent.mike@gmail.com', '56 rue de la Lys', '56908', 'Halluin');

select * from client;

insert into employe(nom, prenom, age, telephone, mail, adresse, code_postal, ville)values('Michel', 'Martin', 67, '0234967865', 'alain.deloin@gmail.com', '56 rue des Flandres', '31908', 'Toulouse');

insert into manager()values('Laurent', 'Pierre', 34, '0934467805', 'l.pierre@gmail.com', '67 rue des Mers', '31908', 'Toulouse');

select * from manager;

alter table client add column date_arrivee Date;

insert into client(nom, prenom, adresse, code_postal, ville, date_arrivee)values('Robert', 'Alain', '5 rue des Champs', '75000', 'Paris', '2021-01-15');

select * from client;

alter table manager add column date_arrivee Date;

alter table employe add column date_arrivee Date;



alter table manager rename to responsable;

alter table responsable change ville city varchar(20);

alter table client change ville city varchar(20);

alter table employe change ville city varchar(20);

create table prospect(
id int primary key not null auto_increment,
nom varchar(30) not null,
prenom varchar(30) not null,
age int check(age>18),
date_contact Date,
telephone varchar(20) default 'Pas de téléphone',
mail varchar(30) default 'Pas de mail',
adresse varchar(40),
code_postal varchar(6),
ville varchar(20),
revenu_annuel decimal(7,2),
constraint NomPrenom unique (nom,prenom)
);

insert into prospect(nom, prenom, age, date_contact, telephone, mail, adresse, code_postal, ville, revenu_annuel)values('Michel', 'Polnareff', 30, '2022-02-05', '0798468754', 'michel@gmail.com', '4 rue du havre', '62260', 'Cauchy', 44000.89);
insert into prospect(nom, prenom, age, date_contact, mail, adresse, code_postal, ville, revenu_annuel)values('Jean', 'Polnareff', 30, '2022-02-05', 'michel@gmail.com', '4 rue du havre', '62260', 'Cauchy', 35000.20);
insert into prospect(nom, prenom, age, date_contact, telephone, adresse, code_postal, ville, revenu_annuel)values('Louis', 'Polnareff', 30, '2022-02-05', '0798468754', '4 rue du havre', '62260', 'Cauchy', 44500.89);

select * from prospect;

alter table prospect change age age int;

insert into prospect(nom,prenom,age,date_contact,telephone,mail,adresse,code_postal,ville,	revenu_annuel)values('Mike', 'Jim', 16, '2022-02-05', '0798468754', 'michel@gmail.com', '4 rue du havre', '62260', 'Cauchy', 44000.89);

alter table responsable drop code_postal;

drop table client, employe;

